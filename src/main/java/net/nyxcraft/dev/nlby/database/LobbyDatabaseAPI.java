package net.nyxcraft.dev.nlby.database;

import net.nyxcraft.dev.nlby.database.dao.LobbyDataDAO;
import net.nyxcraft.dev.nlby.database.entities.LobbyData;
import net.nyxcraft.dev.nyxcore.world.SerializeableLocation;
import org.bukkit.Bukkit;
import org.mongodb.morphia.query.UpdateOperations;

import java.util.ArrayList;

public class LobbyDatabaseAPI {

    public static LobbyData initLobbyData() {
        LobbyData data = LobbyDataDAO.getInstance().find().get();
        if (data == null) {
            data = new LobbyData();
            data.spawn = new SerializeableLocation(Bukkit.getWorlds().get(0).getSpawnLocation());
            LobbyDataDAO.getInstance().save(data);
        }
        return data;
    }

    public static void addAnnouncment(String announcement) {
        UpdateOperations<LobbyData> ops = LobbyDataDAO.getInstance().createUpdateOperations();
        ops.add("announcements", announcement);

        LobbyDataDAO.getInstance().update(LobbyDataDAO.getInstance().createQuery(), ops);
    }

    public static void setAnnouncementInterval(int interval) {
        UpdateOperations<LobbyData> ops = LobbyDataDAO.getInstance().createUpdateOperations();
        ops.set("announcementInterval", interval);

        LobbyDataDAO.getInstance().update(LobbyDataDAO.getInstance().createQuery(), ops);
    }

    public static void resetAnnouncements() {
        UpdateOperations<LobbyData> ops = LobbyDataDAO.getInstance().createUpdateOperations();
        ops.set("announcements", new ArrayList<>());

        LobbyDataDAO.getInstance().update(LobbyDataDAO.getInstance().createQuery(), ops);
    }

}
