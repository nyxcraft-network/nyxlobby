package net.nyxcraft.dev.nlby.database;

import net.nyxcraft.dev.ndb.NyxDB;
import net.nyxcraft.dev.ndb.mongodb.ResourceManager;
import net.nyxcraft.dev.nlby.database.dao.LobbyDataDAO;
import org.mongodb.morphia.Datastore;

public class LobbyDatabaseManager {

    private ResourceManager resourceManager;
    private Datastore datastore;

    public LobbyDatabaseManager() {
        resourceManager = NyxDB.getResourceManager();
        datastore = resourceManager.getDatastore("net.nyxcraft.dev.nlby.database.entities");
        init();
    }

    private void init() {
        datastore.ensureCaps();
        datastore.ensureIndexes();

        register();
    }

    private void register() {
        new LobbyDataDAO(datastore);
    }

}
