package net.nyxcraft.dev.nlby.database.entities;

import net.nyxcraft.dev.nlby.world.Portal;
import net.nyxcraft.dev.nyxcore.world.SerializeableLocation;
import org.bukkit.Material;

public class GameData {

    public Portal portal;
    public SerializeableLocation warp;
    public Material material;

    public GameData() {}

    public GameData(Portal portal, SerializeableLocation warp, Material material) {
        this.portal = portal;
        this.warp = warp;
        this.material = material;
    }

}
