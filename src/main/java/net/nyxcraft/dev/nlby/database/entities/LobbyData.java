package net.nyxcraft.dev.nlby.database.entities;

import net.nyxcraft.dev.nyxcore.world.SerializeableLocation;
import net.nyxcraft.dev.nyxutils.collections.CaseInsensitiveHashMap;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.PostLoad;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity(value = "network_lobby_data", noClassnameStored = true)
public class LobbyData {

    @Id
    public ObjectId id;
    public SerializeableLocation spawn;
    public Map<String, GameData> games = new CaseInsensitiveHashMap<>();
    public List<String> announcements = new ArrayList<>();
    public int announcementInterval = 100;

    public LobbyData() {}

    @PostLoad void postLoad() {
        if (announcements == null) {
            announcements = new ArrayList<>();
        }

        if (announcementInterval <= 0) {
            announcementInterval = 100;
        }
    }
}
