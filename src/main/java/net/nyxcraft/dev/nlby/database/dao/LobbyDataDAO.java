package net.nyxcraft.dev.nlby.database.dao;

import net.nyxcraft.dev.nlby.database.entities.LobbyData;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class LobbyDataDAO extends BasicDAO<LobbyData, ObjectId> {

    private static LobbyDataDAO instance;

    public LobbyDataDAO(Datastore datastore) {
        super(datastore);
        instance = this;
    }

    public static LobbyDataDAO getInstance() {
        return instance;
    }
}
