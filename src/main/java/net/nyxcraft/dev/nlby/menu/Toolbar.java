package net.nyxcraft.dev.nlby.menu;

import net.nyxcraft.dev.nlby.menu.gadgets.GameMenu;
import net.nyxcraft.dev.nyxcore.menu.NetworkPreferencesMenu;
import net.nyxcraft.dev.nyxcore.ui.MenuItem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

import java.util.*;

public class Toolbar {

    private static Map<UUID, Toolbar> toolbars = new HashMap<>();
    private static List<MenuItem> items = new ArrayList<>();

    private UUID uuid;

    static {
        MenuItem gameMenu = new MenuItem(ChatColor.GOLD + "Game Menu", new MaterialData(Material.COMPASS)) {
            @Override
            public void onClick(Player player) {
                new GameMenu().openMenu(player);
            }
        };
        MenuItem store = new MenuItem(ChatColor.DARK_AQUA + "Network Store", new MaterialData(Material.DIAMOND)) {
            @Override
            public void onClick(Player player) {
                player.chat("/buy");
            }
        };
        MenuItem preferences = new MenuItem(ChatColor.RED + "User Preferences", new MaterialData(Material.REDSTONE_COMPARATOR)) {
            @Override
            public void onClick(Player player) {
                NetworkPreferencesMenu.getUserPreferences(player).openMenu(player);
            }
        };

        gameMenu.setSlot(0);
        store.setSlot(1);
        preferences.setSlot(8);

        items.add(gameMenu);
        items.add(store);
        items.add(preferences);
    }

    public Toolbar(Player player) {
        this.uuid = player.getUniqueId();
        setPlayerToolbar();
    }

    public void setPlayerToolbar() {
        Player player = Bukkit.getPlayer(uuid);

        player.getInventory().clear();
        for (MenuItem item : items) {
            player.getInventory().setItem(item.getSlot(), item.getSingleItemStack());
        }
        player.updateInventory();
    }

    public void triggerMenuItem(ItemStack stack) {
        if (stack == null || stack.getData().getItemType() == Material.AIR) {
            return;
        }

        for (MenuItem item : items) {
            if (item.getText().equalsIgnoreCase(stack.getItemMeta().getDisplayName())) {
                item.onClick(Bukkit.getPlayer(uuid));
            }
        }
    }

    public static Toolbar createToolbar(Player player) {
        Toolbar toolbar = new Toolbar(player);
        toolbars.put(player.getUniqueId(), toolbar);
        return toolbar;
    }

    public static Toolbar getToolbar(Player player) {
        return toolbars.get(player.getUniqueId());
    }

}
