package net.nyxcraft.dev.nlby.menu;

import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.player.NyxUser;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Class for initializing and maintaining the game scoreboard.
 * 
 * @author Gideon
 */
public class ScoreboardInstance {

    private static final Map<UUID, ScoreboardInstance> scoreboards = new HashMap<>();
    private static final ScoreboardField WEBSITE = new ScoreboardField(1, ChatColor.RED + "Website");

    private UUID uuid;
    private Scoreboard scoreboard;
    private Objective objective;
    private final ScoreboardField SHARDS = new ScoreboardField(4, ChatColor.GOLD + "SHARDS");
    private final ScoreboardField GEMS = new ScoreboardField(7, ChatColor.YELLOW + "Gems");
    
    public ScoreboardInstance(Player player) {
        this.uuid = player.getUniqueId();
        this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        this.objective = scoreboard.registerNewObjective("nyxcraft_sidebar", "dummy");

        init();
        update();
        showTo(player);
        scoreboards.put(player.getUniqueId(), this);

        for (Player p : Bukkit.getOnlinePlayers()) {
            updatePlayerTeam(p, NyxCore.getUser(p.getUniqueId()));
        }
    }

    private void init() {
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName(ChatColor.GOLD + "Nyxcraft Network!");

        setField(WEBSITE, "www.nyxcraft.net");
    }

    public void update() {
        NyxUser user = NyxCore.getUser(uuid);

        setField(SHARDS, (user == null ? "n/a" : user.getShards() + ""));
        setField(GEMS, (user == null ? "n/a" : user.getGems() + " "));
    }

    public void updatePlayerTeam(Player player, NyxUser user) {
        if (user == null) {
            Bukkit.getPlayer(uuid).sendMessage("user");
            return;
        }
        Team team;
        if ((team = scoreboard.getTeam(user.getRank().name())) == null) {
            team = scoreboard.registerNewTeam(user.getRank().name());
            team.setPrefix(user.getRank().color.toString());
        }

        Team old = scoreboard.getPlayerTeam(player);
        if (old != null) {
            if (old == team) {
                return;
            } else {
                old.removePlayer(player);
            }
        }

        team.addPlayer(player);
    }

    public void removePlayerFromTeam(Player player) {
        Team old = scoreboard.getPlayerTeam(player);
        if (old != null) {
            old.removePlayer(player);
        }
    }
    
    public void showTo(Player player) {
        player.setScoreboard(scoreboard);
    }
    
    public void clearField(ScoreboardField field) {
        setField(field, null);
    }
    
    public void setField(ScoreboardField field, String value) {
        field.set(objective, value);
    }

    public static void cleanup(Player player) {
        scoreboards.remove(player.getUniqueId());
    }
    
    
    public static class ScoreboardField {
    
        private final int score;
        private final String key;
        private String lastvalue;
        
        private ScoreboardField(int score, String key) {
            this.score = score;
            this.key = key;
            this.lastvalue = null;
        }
        
        private void set(Objective objective, String value) {
            value = value == null ? null : value.length() > 16 ? value.substring(0, 16) : value;
            objective.getScore(key).setScore(kscore());
            
            if (lastvalue != null) {
                objective.getScoreboard().resetScores(lastvalue);
            }
            
            this.lastvalue = value;
            
            if (value != null) {
                objective.getScore(value).setScore(vscore());
            }

            if (value != null) {
                objective.getScore(String.format("%1$" + spacer() + "s", " ")).setScore(spacer());
            }
        }
        
        private int kscore() {
            return score;
        }
        
        private int vscore() {
            return score - 1;
        }

        private int spacer() {
            return score + 1;
        }
    }

    public static Map<UUID, ScoreboardInstance> getScoreboards() {
        return scoreboards;
    }
}
