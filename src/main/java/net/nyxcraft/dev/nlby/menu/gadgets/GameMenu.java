package net.nyxcraft.dev.nlby.menu.gadgets;

import net.nyxcraft.dev.nlby.LobbyPlugin;
import net.nyxcraft.dev.nlby.database.entities.GameData;
import net.nyxcraft.dev.nyxcore.ui.Menu;
import net.nyxcraft.dev.nyxcore.ui.MenuItem;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

import java.util.Map;

public class GameMenu extends Menu {
    public GameMenu() {
        super(ChatColor.GOLD + "Game Menu", 1);
        init();
    }

    public void init() {
        for (Map.Entry<String, GameData> entry : LobbyPlugin.getInstance().getLobbyData().games.entrySet()) {
            add(entry.getKey(), entry.getValue());
        }
    }

    public void add(String name, GameData data) {
        if (this.getInventory().getItem(1) == null) {
            this.addMenuItem(create(name, data), 1);
            return;
        }

        if (this.getInventory().getItem(3) == null) {
            this.addMenuItem(create(name, data), 3);
            return;
        }

        if (this.getInventory().getItem(5) == null) {
            this.addMenuItem(create(name, data), 5);
            return;
        }

        if (this.getInventory().getItem(7) == null) {
            this.addMenuItem(create(name, data), 7);
            return;
        }
    }

    public MenuItem create(String name, GameData data) {
        return new MenuItem(ChatColor.GOLD + name, new MaterialData(data.material)) {
            @Override
            public void onClick(Player player) {
                player.teleport(data.warp.getLocation());
            }
        };
    }
}
