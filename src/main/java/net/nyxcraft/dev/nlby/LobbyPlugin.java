package net.nyxcraft.dev.nlby;

import net.nyxcraft.dev.nlby.command.ConfigurationCommands;
import net.nyxcraft.dev.nlby.database.LobbyDatabaseAPI;
import net.nyxcraft.dev.nlby.database.LobbyDatabaseManager;
import net.nyxcraft.dev.nlby.database.entities.LobbyData;
import net.nyxcraft.dev.nlby.listeners.*;
import net.nyxcraft.dev.nlby.listeners.configuration.PlayerListener;
import net.nyxcraft.dev.nlby.listeners.configuration.PortalConfigurationListener;
import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.chat.ActionAnnouncer;
import net.nyxcraft.dev.nyxcore.command.modules.HubCommands;
import net.nyxcraft.dev.nyxcore.world.WorldBorderManager;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class LobbyPlugin extends JavaPlugin {

    private static LobbyPlugin instance;
    private LobbyDatabaseManager databaseManager;
    private LobbyData lobbyData;
    private ActionAnnouncer actionAnnouncer;
    private WorldBorderManager worldBorderManager;

    public void onEnable() {
        instance = this;
        init();
    }

    public static LobbyPlugin getInstance() {
        return instance;
    }

    public void init() {
        databaseManager = new LobbyDatabaseManager();
        loadLobbyData();
        worldBorderManager = new WorldBorderManager(this, lobbyData.spawn.getLocation(), 100);

        registerListeners();
        registerCommands();
        clearMobs();

        Bukkit.getScheduler().scheduleSyncDelayedTask(this, () -> actionAnnouncer = new ActionAnnouncer(NyxCore.getNetworkSettings().announcements.get("lobby") == null ? new ArrayList<>() : NyxCore.getNetworkSettings().announcements.get("lobby"), 5), 20 * 5);
    }

    public void registerListeners() {
        Bukkit.getPluginManager().registerEvents(new HubListener(), this);
        Bukkit.getPluginManager().registerEvents(new ConnectionListener(), this);
        Bukkit.getPluginManager().registerEvents(new MenuListener(), this);
        Bukkit.getPluginManager().registerEvents(new PortalConfigurationListener(), this);
        Bukkit.getPluginManager().registerEvents(new PortalListener(), this);
        Bukkit.getPluginManager().registerEvents(new ChatListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
    }

    public void registerCommands() {
        new ConfigurationCommands();
        new HubCommands();
    }

    public void clearMobs() {
        for (World world : Bukkit.getWorlds()) {
            for (LivingEntity entity : world.getLivingEntities()) {
                if (!(entity instanceof Player)) {
                    entity.remove();
                }
            }
        }
    }

    public LobbyData getLobbyData() {
        return lobbyData;
    }

    public void loadLobbyData() {
        lobbyData = LobbyDatabaseAPI.initLobbyData();
    }

    public WorldBorderManager getWorldBorderManager() {
        return worldBorderManager;
    }

    public ActionAnnouncer getActionAnnouncer() {
        return actionAnnouncer;
    }
}
