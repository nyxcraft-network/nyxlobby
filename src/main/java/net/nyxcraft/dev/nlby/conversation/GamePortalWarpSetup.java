package net.nyxcraft.dev.nlby.conversation;

import net.nyxcraft.dev.nlby.LobbyPlugin;
import net.nyxcraft.dev.nlby.database.dao.LobbyDataDAO;
import net.nyxcraft.dev.nlby.database.entities.GameData;
import net.nyxcraft.dev.nlby.database.entities.LobbyData;
import net.nyxcraft.dev.nlby.world.Portal;
import net.nyxcraft.dev.nyxcore.world.SerializeableLocation;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.conversations.*;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class GamePortalWarpSetup implements IConversation, ConversationAbandonedListener {

    private static final List<String> materials = new ArrayList();

    static {
        for (Material material : Material.values()) {
            materials.add(material.name().toLowerCase());
        }
    }

    private ConversationFactory factory;
    private boolean active = false;

    // Data
    private Block portalCornerOne;
    private Block portalCornerTwo;
    private String destination;
    private Location warpLocation;
    private String id;
    private Material material;

    private boolean selectedFirstCorner = false;
    private boolean selectedSecondCorner = false;

    public GamePortalWarpSetup() {
        this.factory = new ConversationFactory(LobbyPlugin.getInstance())
                .withModality(true)
                .withPrefix(new NullConversationPrefix())
                .withFirstPrompt(new PortalCornerPrompt(this))
                .withEscapeSequence("quit")
                .withTimeout(90)
                .thatExcludesNonPlayersWithMessage("Go away evil console!")
                .addConversationAbandonedListener(this);
    }

    @Override
    public void beginConversation(Player player) {
        active = true;
        player.beginConversation(factory.buildConversation(player));
    }

    @Override
    public void conversationAbandoned(ConversationAbandonedEvent conversationAbandonedEvent) {
        active = false;
    }

    public boolean isSelectedSecondCorner() {
        return selectedSecondCorner;
    }

    public void setSelectedSecondCorner(boolean selectedSecondCorner) {
        this.selectedSecondCorner = selectedSecondCorner;
    }

    public boolean isSelectedFirstCorner() {
        return selectedFirstCorner;
    }

    public void setSelectedFirstCorner(boolean selectedFirstCorner) {
        this.selectedFirstCorner = selectedFirstCorner;
    }

    public Block getPortalCornerOne() {
        return portalCornerOne;
    }

    public void setPortalCornerOne(Block portalCornerOne) {
        this.portalCornerOne = portalCornerOne;
    }

    public Block getPortalCornerTwo() {
        return portalCornerTwo;
    }

    public void setPortalCornerTwo(Block portalCornerTwo) {
        this.portalCornerTwo = portalCornerTwo;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    private class PortalCornerPrompt extends FixedSetPrompt {

        private GamePortalWarpSetup setup;

        public PortalCornerPrompt(GamePortalWarpSetup setup) {
            super("DONE", "FINISHED", "END");
            this.setup = setup;
        }

        @Override
        protected Prompt acceptValidatedInput(ConversationContext conversationContext, String s) {
            if (portalCornerOne != null) {
                if (selectedFirstCorner == false) {
                    selectedFirstCorner = true;
                }
            }

            System.out.println("True: " + selectedFirstCorner + " Null: " + (portalCornerOne == null));

            if (portalCornerTwo != null) {
                if (selectedSecondCorner == false) {
                    selectedSecondCorner = true;
                }
            }

            if (selectedFirstCorner && selectedSecondCorner) {
                return new PortalDestinationPrompt(setup);
            }

            return this;
        }

        @Override
        public String getPromptText(ConversationContext context) {
            return "Type DONE once you've selected the " + (selectedFirstCorner == false ? "first" : "second") + " portal corner.";
        }
    }

    private class PortalDestinationPrompt extends StringPrompt {

        private GamePortalWarpSetup setup;

        public PortalDestinationPrompt(GamePortalWarpSetup setup) {
            this.setup = setup;
        }

        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return "Enter the destination for this portal.";
        }

        @Override
        public Prompt acceptInput(ConversationContext conversationContext, String s) {
            setup.destination = s;

            if (setup.destination == null || setup.destination.equals("")) {
                return this;
            }

            return new WarpLocationPrompt(setup);
        }
    }

    private class WarpLocationPrompt extends FixedSetPrompt {

        private GamePortalWarpSetup setup;

        public WarpLocationPrompt(GamePortalWarpSetup setup) {
            super("DONE", "FINISHED", "END");
            this.setup = setup;
        }

        @Override
        protected Prompt acceptValidatedInput(ConversationContext conversationContext, String s) {
            setup.warpLocation = ((Player)conversationContext.getForWhom()).getLocation();
            return new SetIdPrompt(setup);
        }

        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return "Walk to a location and face the direction you want players to spawn for this warp, then type DONE!";
        }
    }

    private class SetIdPrompt extends StringPrompt {

        private GamePortalWarpSetup setup;

        public SetIdPrompt(GamePortalWarpSetup setup) {
            this.setup = setup;
        }

        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return "Enter the id for this portal and warp.";
        }

        @Override
        public Prompt acceptInput(ConversationContext conversationContext, String s) {
            setup.id = s;

            if (setup.id == null || setup.id.equals("")) {
                return this;
            }

            return new SetMaterialPrompt(setup);
        }
    }

    private class SetMaterialPrompt extends FixedSetPrompt {

        private GamePortalWarpSetup setup;

        public SetMaterialPrompt(GamePortalWarpSetup setup) {
            super(materials.toArray(new String[materials.size()]));
            this.setup = setup;
        }

        @Override
        protected Prompt acceptValidatedInput(ConversationContext conversationContext, String s) {
            Material material = Material.valueOf(s.toUpperCase());

            if (material == null) {
                return this;
            }

            setup.material = material;
            return new CompletePrompt(setup);
        }

        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return "Specify the material to use in menus for the warp.";
        }

        @Override
        protected boolean isInputValid(ConversationContext context, String input) {
            return super.isInputValid(context, input.toLowerCase());
        }
    }

    private class CompletePrompt extends MessagePrompt {

        private GamePortalWarpSetup setup;

        public CompletePrompt(GamePortalWarpSetup setup) {
            this.setup = setup;
        }

        @Override
        protected Prompt getNextPrompt(ConversationContext conversationContext) {
            configure();
            active = false;
            return END_OF_CONVERSATION;
        }

        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return "The portal has successfully been registered!";
        }

        public void configure() {
            LobbyData data = LobbyPlugin.getInstance().getLobbyData();

            Portal portal = new Portal(setup.portalCornerOne, setup.portalCornerTwo, setup.destination);
            SerializeableLocation warp = new SerializeableLocation(setup.warpLocation);
            data.games.put(setup.id, new GameData(portal, warp, setup.material));

            LobbyDataDAO.getInstance().save(data);
        }
    }

}
