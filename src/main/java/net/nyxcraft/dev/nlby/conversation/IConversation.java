package net.nyxcraft.dev.nlby.conversation;

import org.bukkit.entity.Player;

public interface IConversation {

    public void beginConversation(Player player);

}
