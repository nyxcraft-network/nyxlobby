package net.nyxcraft.dev.nlby.world;

import org.bukkit.Location;
import org.bukkit.block.Block;

public class Portal {
    // First corner selection
    private int x1;
    private int y1;
    private int z1;
    // Second corner selection
    private int x2;
    private int y2;
    private int z2;
    // Portal metadata
    private String world;
    private String destination;

    public Portal() {}

    public Portal(Block block, Block block2, String destination) {
        setLocation(block.getLocation(), block2.getLocation());
        this.destination = destination;
    }

    public void setLocation(Location cornerOne, Location cornerTwo) {
        world = cornerOne.getWorld().getName();

        x1 = (cornerOne.getBlockX() < cornerTwo.getBlockX()) ? cornerOne.getBlockX() : cornerTwo.getBlockX();
        x2 = (cornerOne.getBlockX() > cornerTwo.getBlockX()) ? cornerOne.getBlockX() : cornerTwo.getBlockX();
        y1 = (cornerOne.getBlockY() < cornerTwo.getBlockY()) ? cornerOne.getBlockY() : cornerTwo.getBlockY();
        y2 = (cornerOne.getBlockY() > cornerTwo.getBlockY()) ? cornerOne.getBlockY() : cornerTwo.getBlockY();
        z1 = (cornerOne.getBlockZ() < cornerTwo.getBlockZ()) ? cornerOne.getBlockZ() : cornerTwo.getBlockZ();
        z2 = (cornerOne.getBlockZ() > cornerTwo.getBlockZ()) ? cornerOne.getBlockZ() : cornerTwo.getBlockZ();
    }

    public int getX1() {
        return x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public int getY1() {
        return y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public int getZ1() {
        return z1;
    }

    public void setZ1(int z1) {
        this.z1 = z1;
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    public int getZ2() {
        return z2;
    }

    public void setZ2(int z2) {
        this.z2 = z2;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public boolean containsBlock(Block block) {
        for (int x = x1; x <= x2; x++) {
            for (int z = z1; z <= z2; z++) {
                for (int y = y1; y <= y2; y++) {
                    if (block.getX() == x && block.getY() == y && block.getZ() == z) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public boolean blockMatchesPortalChunk(Block block) {
        int lowChunkX = x1 >> 4;
        int lowChunkZ = z1 >> 4;
        int highChunkX = x2 >> 4;
        int highChunkZ = z2 >> 4;

        int blockChunkX = block.getChunk().getX();
        int blockChunkZ = block.getChunk().getZ();

        if ((blockChunkX >= lowChunkX && blockChunkX <= highChunkX) && (blockChunkZ >= lowChunkZ && blockChunkZ <= highChunkZ)) {
            return true;
        }

        return false;
    }
}
