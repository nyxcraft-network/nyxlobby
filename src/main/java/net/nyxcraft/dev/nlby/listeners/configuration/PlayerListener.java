package net.nyxcraft.dev.nlby.listeners.configuration;

import net.nyxcraft.dev.nlby.menu.ScoreboardInstance;
import net.nyxcraft.dev.nyxcore.event.SetPlayerRankEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.UUID;

public class PlayerListener implements Listener {

    @EventHandler
    public void onSetPlayerRank(SetPlayerRankEvent event) {
        Player player;
        if ((player = Bukkit.getPlayer(UUID.fromString(event.getProfile().uuid))) != null) {
            for (ScoreboardInstance instance : ScoreboardInstance.getScoreboards().values()) {
                instance.removePlayerFromTeam(player);
            }
        }
    }

}
