package net.nyxcraft.dev.nlby.listeners;

import net.nyxcraft.dev.nlby.LobbyPlugin;
import net.nyxcraft.dev.nlby.database.entities.GameData;
import net.nyxcraft.dev.nlby.world.Portal;
import net.nyxcraft.dev.nyxcore.database.redis.NetworkUtil;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PortalListener implements Listener {

    public Map<UUID, Portal> playersInPortals = new HashMap<>();

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerMove(PlayerMoveEvent event) {
        if (playersInPortals.containsKey(event.getPlayer().getUniqueId())) {
            Portal portal = playersInPortals.get(event.getPlayer().getUniqueId());
            if (portal.containsBlock(event.getTo().getBlock()) == false) {
                playersInPortals.remove(event.getPlayer().getUniqueId());
            }
        } else {
            Block block = event.getTo().getBlock();
            for (GameData game : LobbyPlugin.getInstance().getLobbyData().games.values()) {
                if (game.portal.blockMatchesPortalChunk(block)) {
                    if (game.portal.containsBlock(block)) {
                        playersInPortals.put(event.getPlayer().getUniqueId(), game.portal);
                        NetworkUtil.send(event.getPlayer(), game.portal.getDestination());
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerQuit(PlayerQuitEvent event) {
        if (playersInPortals.containsKey(event.getPlayer().getUniqueId())) {
            playersInPortals.remove(event.getPlayer().getUniqueId());
        }
    }

}
