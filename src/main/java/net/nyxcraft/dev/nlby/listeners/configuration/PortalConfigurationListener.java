package net.nyxcraft.dev.nlby.listeners.configuration;

import net.nyxcraft.dev.nlby.conversation.GamePortalWarpSetup;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PortalConfigurationListener implements Listener {

    private static Map<UUID, GamePortalWarpSetup> portalConfigurations = new HashMap<>();

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = false)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (portalConfigurations.containsKey(event.getPlayer().getUniqueId())) {
            GamePortalWarpSetup setup = portalConfigurations.get(event.getPlayer().getUniqueId());
            if (setup.isActive()) {
                if (!setup.isSelectedFirstCorner()) {
                    setup.setPortalCornerOne(event.getClickedBlock());
                } else if (!setup.isSelectedSecondCorner()) {
                    setup.setPortalCornerTwo(event.getClickedBlock());
                }
            } else {
                portalConfigurations.remove(event.getPlayer().getUniqueId());
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerQuit(PlayerQuitEvent event) {
        if (portalConfigurations.containsKey(event.getPlayer().getUniqueId())) {
            portalConfigurations.remove(event.getPlayer().getUniqueId());
        }
    }

    public static Map<UUID, GamePortalWarpSetup> getPortalConfigurations() {
        return portalConfigurations;
    }
}
