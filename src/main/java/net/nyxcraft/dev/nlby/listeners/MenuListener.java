package net.nyxcraft.dev.nlby.listeners;

import net.nyxcraft.dev.nlby.menu.Toolbar;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

import javax.tools.Tool;

public class MenuListener implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerJoin(PlayerJoinEvent event) {
        if (event.getPlayer().getGameMode() == GameMode.SURVIVAL || event.getPlayer().getGameMode() == GameMode.ADVENTURE) {
            Toolbar.createToolbar(event.getPlayer());
        } if (event.getPlayer().getGameMode() == GameMode.CREATIVE) {
            event.getPlayer().getInventory().clear();
            event.getPlayer().updateInventory();
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = false)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getPlayer().getGameMode() == GameMode.CREATIVE) {
            return;
        }

        ItemStack item = event.getPlayer().getItemInHand();

        if (item == null) {
            return;
        }

        Toolbar toolbar = Toolbar.getToolbar(event.getPlayer());

        if (toolbar == null) {
            toolbar = Toolbar.createToolbar(event.getPlayer());
        }

        toolbar.triggerMenuItem(item);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onInventoryClick(InventoryClickEvent event) {
        if(event.getWhoClicked().getGameMode() == GameMode.SURVIVAL || event.getWhoClicked().getGameMode() == GameMode.ADVENTURE) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerGameModeChange(PlayerGameModeChangeEvent event) {
        if (event.getNewGameMode() == GameMode.CREATIVE) {
            event.getPlayer().getInventory().clear();
            event.getPlayer().updateInventory();
        }

        if (event.getNewGameMode() == GameMode.SURVIVAL || event.getNewGameMode() == GameMode.ADVENTURE) {
            if (Toolbar.getToolbar(event.getPlayer()) != null) {
                Toolbar.getToolbar(event.getPlayer()).setPlayerToolbar();
            } else {
                Toolbar.createToolbar(event.getPlayer());
            }
        }
    }
}
