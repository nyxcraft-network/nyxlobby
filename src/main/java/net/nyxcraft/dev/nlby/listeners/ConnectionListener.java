package net.nyxcraft.dev.nlby.listeners;

import net.nyxcraft.dev.nlby.LobbyPlugin;
import net.nyxcraft.dev.nlby.menu.ScoreboardInstance;
import net.nyxcraft.dev.nyxcore.event.LoginSuccessEvent;
import net.nyxcraft.dev.nyxutils.chat.MessageService;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectionListener implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onLoginSuccess(LoginSuccessEvent event) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(LobbyPlugin.getInstance(), () -> new ScoreboardInstance(Bukkit.getPlayer(event.getProfile().getUuid())), 20);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event) {
        // Teleport player to spawn
        event.getPlayer().teleport(LobbyPlugin.getInstance().getLobbyData().spawn.getLocation());

        // Show welcome subtitle to player
        String title = ChatColor.translateAlternateColorCodes('&', "&2Welcome &c" + event.getPlayer().getName());
        String subtitle = ChatColor.translateAlternateColorCodes('&', "&cto &2Nyxcraft &cNetwork&2!");
        MessageService.sendFullTitleAnnouncement(event.getPlayer(), title, subtitle, 0, 100, 50);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerQuit(PlayerQuitEvent event) {
        ScoreboardInstance.cleanup(event.getPlayer());

        for (ScoreboardInstance instance : ScoreboardInstance.getScoreboards().values()) {
            instance.removePlayerFromTeam(event.getPlayer());
        }
    }
}
