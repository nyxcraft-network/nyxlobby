package net.nyxcraft.dev.nlby.listeners;

import net.nyxcraft.dev.nlby.LobbyPlugin;
import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.event.AnnouncementsUpdatedEvent;
import net.nyxcraft.dev.nyxcore.player.Rank;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e) {
        Rank rank = NyxCore.getUser(e.getPlayer().getUniqueId()).getRank();
        String format ="";
        if (rank != Rank.DEFAULT) {
            format = ChatColor.DARK_GRAY + "[" + rank.color + rank.prefix + ChatColor.DARK_GRAY + "] ";
        }

        if (rank.ordinal() >= Rank.HELPER.ordinal()) {
            format += ChatColor.WHITE + "%s: %s";
        } else {
            format += ChatColor.GRAY + "%s: %s";
        }

        e.setFormat(format);
    }

    @EventHandler
    public void onAnnouncementsUpdated(AnnouncementsUpdatedEvent event) {
        LobbyPlugin.getInstance().getActionAnnouncer().setMessages(event.getNetworkSettings().announcements.get("lobby"));
    }

}
