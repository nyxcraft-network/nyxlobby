package net.nyxcraft.dev.nlby.command;

import net.nyxcraft.dev.nlby.LobbyPlugin;
import net.nyxcraft.dev.nlby.conversation.GamePortalWarpSetup;
import net.nyxcraft.dev.nlby.database.dao.LobbyDataDAO;
import net.nyxcraft.dev.nlby.listeners.configuration.PortalConfigurationListener;
import net.nyxcraft.dev.nyxcore.chat.MessageFormatter;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import net.nyxcraft.dev.nyxcore.player.Rank;
import net.nyxcraft.dev.nyxcore.world.SerializeableLocation;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ConfigurationCommands {

    public ConfigurationCommands() {
        CommandRegistry.registerPlayerCommand(LobbyPlugin.getInstance(), "cp", Rank.ADMINISTRATOR, ConfigurationCommands::configurePortals);
        CommandRegistry.registerPlayerCommand(LobbyPlugin.getInstance(), "cs", Rank.ADMINISTRATOR, ConfigurationCommands::configureSpawn);
        CommandRegistry.registerUniversalCommand(LobbyPlugin.getInstance(), "update", Rank.ADMINISTRATOR, ConfigurationCommands::update);
    }

    public static void configurePortals(Player sender, String[] args) {
        GamePortalWarpSetup setup = new GamePortalWarpSetup();
        PortalConfigurationListener.getPortalConfigurations().put(sender.getUniqueId(), setup);
        setup.beginConversation(sender);
    }

    public static void configureSpawn(Player sender, String[] args) {
        LobbyPlugin.getInstance().getLobbyData().spawn = new SerializeableLocation(sender.getLocation());
        sender.getLocation().getWorld().setSpawnLocation(sender.getLocation().getBlockX(), sender.getLocation().getBlockY(), sender.getLocation().getBlockZ());
        LobbyDataDAO.getInstance().save(LobbyPlugin.getInstance().getLobbyData());
        LobbyPlugin.getInstance().getWorldBorderManager().update(LobbyPlugin.getInstance().getLobbyData().spawn.getLocation(), 100);
        MessageFormatter.sendSuccessMessage(sender, "The lobby spawn has been set to your position.");
    }

    public static void update(CommandSender sender, String[] args) {
        LobbyPlugin.getInstance().loadLobbyData();
        MessageFormatter.sendSuccessMessage(sender, "Lobby data has been reloaded.");
    }
}
